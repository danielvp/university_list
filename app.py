import json
from collections import defaultdict
from flask import Flask, request, make_response, current_app
from pytrie import Trie
from datetime import timedelta
from functools import update_wrapper
import uuid
import requests
import heapq
app = Flask(__name__)

data = list()
country_index = defaultdict(list)
country_codes = dict()
name_index = dict()

@app.route("/universities/all")
def universities():
    return getJsonData()

def getJsonData():
    return json.dumps(open('world_universities_and_domains.json').read());

@app.errorhandler(500)
def internal_error(error):
    print "error is "+ repr(error)

def crossdomain(origin=None, methods=None, headers=None,
        max_age=21600, attach_to_all=True,
        automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers
            # h['CORS_HEADERS']='Content-Type'
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Headers'] = 'Content-Type' #headers
            if headers is not None:
                h['Access-Control-Allow-Headers'] = 'Content-Type' #headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

@app.route("/universities/country_codes",methods=['GET', 'OPTIONS'])
@crossdomain(origin="*")
def country_codes_route():
    if not data_loaded:
        load_local_data()
    filtered = [ {'country_code':k, 'country':country_codes[k] } for k in sorted(country_codes)]

    return json.dumps({'body':filtered})

data_loaded = False

@app.route("/universities/search",methods=['GET', 'OPTIONS'])
@crossdomain(origin="*")
def search():
    if not data_loaded:
        load_local_data()
    limit = request.args.get('limit')
    country = request.args.get('country')
    name = request.args.get('name')
    filtered = data

    print 'name is '+ str(limit)
    # limited = {k: data.pop() for k in range(1, 11)}
    # print filtered
    if limit:
        print limit
        filtered = [data[k] for k in range(1, int(request.args.get('limit')))]

    # print limited
    if name and country:
        name = name.lower()
        country = country.lower()
        name_filtered = prefix_tree.values(prefix=name)
        country_filtered = country_index[country]
        filtered = [i for i in name_filtered if i['name'] in [_i['name'] for _i in country_filtered]]

    elif name:
        name = name.lower()
        # filtered = prefix_tree.values(prefix=name)
        print len(prefix_tree.values(prefix=name))
        if limit and len(prefix_tree.values(prefix=name)) > 10:
            filtered = [prefix_tree.values(prefix=name)[k] for k in range(1, int(limit))]
        else:
            filtered = prefix_tree.values(prefix=name)


    elif country:
        country = country.lower()
        # filtered = country_index[country]
        if limit:
            filtered = [country_index[country][k] for k in range(1, int(limit))]
        else:
            filtered = country_index[country]

    # if limit:
    #     print limit
    #     filtered = [data[k] for k in range(1, int(limit))]


    return json.dumps(filtered)

data_loaded = False

def load_local_data():
    print "load local data"
    global data_loaded, prefix_tree, data, country_index, country_codes, name_index
    response = open('world_universities_and_domains.json')
    data = json.load(response)
    # limited = {k: data[k] for k in heapq.nsmallest(data, limit, key=int)}
    # print "load local data2 "+ data
    for i in data:
        country_index[i["country"].lower()].append(i)
        country_codes[i["alpha_two_code"]] = i["country"]
        name_index[i['name'].lower()] = i
        splitted = i['name'].split(" ")
        if len(splitted) > 1:
            for splitted_name in splitted[1:]:
                name_index[splitted_name.lower() + str(uuid.uuid1())] = i
    prefix_tree = Trie(**name_index)
    data_loaded = True


@app.route('/')
def index():

    if not data_loaded:
        load_local_data()

    data = {'author': {'name': 'hipo', 'website': 'http://hipolabs.com'},
            'example': 'http://universities.hipolabs.com/search?name=middle&country=Turkey',
            'github': 'https://github.com/Hipo/university-domains-list'}
    return json.dumps(data)

if __name__ == "__main__":
    app.run(host="139.59.146.61", debug=False)
